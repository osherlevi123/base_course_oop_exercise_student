package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public abstract class AerialVehicle implements AerialVehicleService {

    protected Coordinates base;
    protected int hoursSinceLastFix;
    protected FlightStatusTypes flightStatus;
    protected int MAX_HOURS_AFTER_REPAIR;

    public AerialVehicle(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus) {
        this.base = base;
        this.hoursSinceLastFix = (hoursSinceLastFix < 0) ? 0 : hoursSinceLastFix;
        this.flightStatus = flightStatus;
    }

    @Override
    public void flyTo(Coordinates destination) {
        if (this.flightStatus == FlightStatusTypes.READY_FOR_FLIGHT) {
            System.out.println("Flying to: " + getCoordinates(destination));
        } else if (this.flightStatus == FlightStatusTypes.NOT_READY) {
            System.out.println(this.getClass().getSimpleName() + " is not ready to fly");
        }
        this.setStatusType(FlightStatusTypes.IN_AIR);
    }

    @Override
    public void check() {
        if (this.hoursSinceLastFix >= this.MAX_HOURS_AFTER_REPAIR) {
            this.setStatusType(FlightStatusTypes.NOT_READY);
            this.repair();
        } else {
            this.setStatusType(FlightStatusTypes.READY_FOR_FLIGHT);
        }
    }

    @Override
    public void land(Coordinates destination) {
        System.out.println("Landing on: " + getCoordinates(destination));
        this.check();
    }

    @Override
    public void repair() {
        this.hoursSinceLastFix = 0;
        this.setStatusType(FlightStatusTypes.READY_FOR_FLIGHT);
    }

    protected String getCoordinates(Coordinates destination) {
        return (destination.getLatitude().toString() + " , " + destination.getLongitude().toString());
    }

    public Coordinates base() {
        return this.base;
    }


    public void setStatusType(FlightStatusTypes statusType) {
        this.flightStatus = statusType;
    }

}
