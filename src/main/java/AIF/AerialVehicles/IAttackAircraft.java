package AIF.AerialVehicles;

public interface IAttackAircraft {
    enum rocketTypes {
        PYTHON,
        AMRAM,
        SPICE250
    }

    int rocketAmounts();

    rocketTypes rocket();

}