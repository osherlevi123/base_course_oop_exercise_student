package AIF.AerialVehicles;

public interface IVerificationAircraft {
   enum CameraTypes {
    Regular,
    Thermal,
    NightVision
  }

  CameraTypes camera();
}
