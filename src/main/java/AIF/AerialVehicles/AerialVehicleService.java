package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public interface AerialVehicleService {
    void flyTo(Coordinates coordinates);
    void land(Coordinates coordinates);
    void check();
    void repair();
}
