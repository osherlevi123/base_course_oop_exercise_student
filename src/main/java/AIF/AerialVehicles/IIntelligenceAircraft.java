package AIF.AerialVehicles;

public interface IIntelligenceAircraft {
    enum sensorTypes {
        InfraRed,
        Elint
    }

    sensorTypes sensor();
}