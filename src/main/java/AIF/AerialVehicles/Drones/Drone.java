package AIF.AerialVehicles.Drones;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IIntelligenceAircraft;
import AIF.Entities.Coordinates;

public abstract class Drone extends AerialVehicle implements IIntelligenceAircraft {
    protected sensorTypes sensorType;

    public Drone(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, sensorTypes sensorType) {
        super(base, hoursSinceLastFix, flightStatus);
        this.sensorType = sensorType;
    }

    public String hoverOverLocation(Coordinates destination) {
        String hoverStation = "Hovering over: " + getCoordinates(destination);
        this.flightStatus = FlightStatusTypes.IN_AIR;
        System.out.println(hoverStation);
        return (hoverStation);
    }

    @Override
    public sensorTypes sensor() {
        return this.sensorType;
    }
}
