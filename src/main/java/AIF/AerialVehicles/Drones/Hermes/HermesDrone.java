package AIF.AerialVehicles.Drones.Hermes;

import AIF.AerialVehicles.Drones.Drone;
import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IVerificationAircraft;
import AIF.Entities.Coordinates;

public abstract class HermesDrone extends Drone implements IVerificationAircraft {
    private CameraTypes camera;

    public HermesDrone(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, sensorTypes sensorType, CameraTypes cameraType) {
        super(base, hoursSinceLastFix, flightStatus, sensorType);
        MAX_HOURS_AFTER_REPAIR = 100;
        camera = cameraType;
    }

    @Override
    public CameraTypes camera() {
        return this.camera;
    }

}
