package AIF.AerialVehicles.Drones.Hermes;

import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IAttackAircraft;
import AIF.Entities.Coordinates;

public class Kochav extends HermesDrone implements IAttackAircraft {
    private rocketTypes rocket;
    private int rocketAmount;

    public Kochav(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, rocketTypes rocket, CameraTypes camera, sensorTypes sensor, int rocketAmount) {
        super(base, hoursSinceLastFix, flightStatus, sensor, camera);
        this.rocket = rocket;
        this.rocketAmount = rocketAmount;
    }

    @Override
    public int rocketAmounts() {
        return this.rocketAmount;
    }

    @Override
    public rocketTypes rocket() {
        return this.rocket;
    }


}
