package AIF.AerialVehicles.Drones.Haron;

import AIF.AerialVehicles.FlightStatusTypes;
import AIF.Entities.Coordinates;

public class Eitan extends HaronDrone {

    public Eitan(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, rocketTypes rocket, sensorTypes sensor, int rocketAmount) {
        super(base, hoursSinceLastFix, flightStatus, sensor, rocket, rocketAmount);

    }

}
