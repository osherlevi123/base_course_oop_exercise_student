package AIF.AerialVehicles.Drones.Haron;

import AIF.AerialVehicles.Drones.Drone;
import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IAttackAircraft;
import AIF.Entities.Coordinates;

public abstract class HaronDrone extends Drone implements IAttackAircraft {
    protected rocketTypes rocket;
    protected int rocketAmount;

    public HaronDrone(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, sensorTypes sensor, rocketTypes rocket, int rocketAmount) {
        super(base, hoursSinceLastFix, flightStatus, sensor);
        this.rocket = rocket;
        this.rocketAmount = rocketAmount;
        this.MAX_HOURS_AFTER_REPAIR = 150;
    }

    @Override
    public int rocketAmounts() {
        return this.rocketAmount;
    }


    @Override
    public rocketTypes rocket() {
        return this.rocket;
    }


}
