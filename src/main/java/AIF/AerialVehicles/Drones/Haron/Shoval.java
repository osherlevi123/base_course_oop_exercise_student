package AIF.AerialVehicles.Drones.Haron;

import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IAttackAircraft;
import AIF.AerialVehicles.IVerificationAircraft;
import AIF.Entities.Coordinates;

public class Shoval extends HaronDrone implements IAttackAircraft, IVerificationAircraft {

    private CameraTypes camera;

    public Shoval(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, rocketTypes rocket, CameraTypes camera, sensorTypes sensor, int rocketAmount) {
        super(base, hoursSinceLastFix, flightStatus, sensor, rocket, rocketAmount);
        this.camera = camera;

    }

    @Override
    public CameraTypes camera() {
        return this.camera;
    }


}

