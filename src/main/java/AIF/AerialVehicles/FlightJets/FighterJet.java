package AIF.AerialVehicles.FlightJets;


import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IAttackAircraft;
import AIF.Entities.Coordinates;

public abstract class FighterJet extends AerialVehicle implements IAttackAircraft {

    protected rocketTypes rocket;
    protected int rocketAmount;

    public FighterJet(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, rocketTypes rocket, int rocketAmount) {
        super(base, hoursSinceLastFix, flightStatus);
        this.rocket = rocket;
        this.rocketAmount = rocketAmount;
        this.MAX_HOURS_AFTER_REPAIR = 250;
    }

    @Override
    public int rocketAmounts() {
        return this.rocketAmount;
    }

    @Override
    public rocketTypes rocket() {
        return this.rocket;
    }

}
