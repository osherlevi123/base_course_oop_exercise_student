package AIF.AerialVehicles.FlightJets;

import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IIntelligenceAircraft;
import AIF.Entities.Coordinates;

public class F15 extends FighterJet implements IIntelligenceAircraft {

    private sensorTypes sensor;

    public F15(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, rocketTypes rocket, sensorTypes sensor, int rocketAmount) {
        super(base, hoursSinceLastFix, flightStatus, rocket, rocketAmount);
        this.sensor = sensor;
    }

    @Override
    public sensorTypes sensor() {
        return this.sensor;
    }


}
