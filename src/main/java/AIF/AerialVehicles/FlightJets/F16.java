package AIF.AerialVehicles.FlightJets;

import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IVerificationAircraft;
import AIF.Entities.Coordinates;

public class F16 extends FighterJet implements IVerificationAircraft {

    private CameraTypes camera;

    public F16(Coordinates base, int hoursSinceLastFix, FlightStatusTypes flightStatus, CameraTypes camera, rocketTypes rocketTypes, int rocketAmount) {
        super(base, hoursSinceLastFix, flightStatus, rocketTypes, rocketAmount);
        this.camera = camera;

    }

    @Override
    public CameraTypes camera() {
        return this.camera;
    }


}
