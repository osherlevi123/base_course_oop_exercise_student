package AIF.AerialVehicles;

public enum FlightStatusTypes {
    READY_FOR_FLIGHT,
    NOT_READY,
    IN_AIR
}
