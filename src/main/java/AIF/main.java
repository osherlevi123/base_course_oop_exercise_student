package AIF;

import AIF.AerialVehicles.Drones.Hermes.Kochav;
import AIF.AerialVehicles.FlightJets.F15;
import AIF.AerialVehicles.FlightJets.F16;
import AIF.AerialVehicles.FlightStatusTypes;
import AIF.AerialVehicles.IAttackAircraft;
import AIF.AerialVehicles.IIntelligenceAircraft;
import AIF.AerialVehicles.IVerificationAircraft;
import AIF.Entities.Coordinates;
import AIF.Missions.AttackMission;
import AIF.Missions.BdaMission;
import AIF.Missions.IntelligenceMission;

public class main {

    public static void main(String[] args) {
        Coordinates coordinate = new Coordinates(34.0, 31.1);
        Coordinates base = new Coordinates(34.0, 31.2);

        Kochav aircraft =
                new Kochav(base, 200, FlightStatusTypes.READY_FOR_FLIGHT, IAttackAircraft.rocketTypes.AMRAM, IVerificationAircraft.CameraTypes.NightVision, IIntelligenceAircraft.sensorTypes.Elint, 5);

        F15 f15 =
                new F15(base, 50, FlightStatusTypes.IN_AIR, IAttackAircraft.rocketTypes.PYTHON, IIntelligenceAircraft.sensorTypes.InfraRed, 9);
        F16 f16 =
                new F16(base, 20, FlightStatusTypes.NOT_READY , IVerificationAircraft.CameraTypes.NightVision, IAttackAircraft.rocketTypes.SPICE250, 9);


        try {
            BdaMission bda = new BdaMission("Shimi", f16, coordinate, "Syria");
            bda.begin();
            System.out.println(bda.executeMission());

            IntelligenceMission intelligence =
                    new IntelligenceMission("Yuval", aircraft, coordinate, "Meitar");
            intelligence.begin();
            intelligence.cancel();
            System.out.println(intelligence.executeMission());


            AttackMission attack = new AttackMission("Gal", f15, coordinate, "Ahmed");
            attack.begin();
            attack.finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
