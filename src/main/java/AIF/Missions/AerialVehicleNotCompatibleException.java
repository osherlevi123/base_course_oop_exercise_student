package AIF.Missions;

public class AerialVehicleNotCompatibleException extends Exception {
    public AerialVehicleNotCompatibleException(String message) {
        super(message);
    }
}
