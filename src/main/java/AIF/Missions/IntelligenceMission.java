package AIF.Missions;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.IIntelligenceAircraft;
import AIF.Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(String pilotName, AerialVehicle aircraft, Coordinates destination, String region) throws Exception {
        super(pilotName, aircraft, destination);
        if (!(aircraft instanceof IIntelligenceAircraft)) {
            throw new AerialVehicleNotCompatibleException("not compatible aircraft");
        }
        this.region = region;
    }

    @Override
    public String executeMission() {
        return (this.pilotName
                + ": "
                + this.aircraft.getClass().getSimpleName()
                + " Collecting Data in "
                + this.region
                + " with: "
                + ((IIntelligenceAircraft) this.aircraft).sensor());
    }

}
