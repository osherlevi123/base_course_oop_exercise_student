package AIF.Missions;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.IAttackAircraft;
import AIF.Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(String pilotName, AerialVehicle aircraft, Coordinates destination, String target) throws Exception {
        super(pilotName, aircraft, destination);
        if (!(aircraft instanceof IAttackAircraft)) {
            throw new AerialVehicleNotCompatibleException("not compatible aircraft");
        }
        this.target = target;
    }

    @Override
    public String executeMission() {
        return (this.pilotName
                + ": "
                + this.aircraft.getClass().getSimpleName()
                + " Attacking "
                + this.target
                + " with: "
                + ((IAttackAircraft) this.aircraft).rocket()
                + "X" + ((IAttackAircraft) this.aircraft).rocketAmounts());
    }
}
