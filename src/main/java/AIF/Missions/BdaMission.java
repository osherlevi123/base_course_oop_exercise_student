package AIF.Missions;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.IVerificationAircraft;
import AIF.Entities.Coordinates;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(String pilotName, AerialVehicle aircraft, Coordinates destination, String objective) throws Exception {
        super(pilotName, aircraft, destination);
        if (!(aircraft instanceof IVerificationAircraft)) {
            throw new AerialVehicleNotCompatibleException("not compatible aircraft");
        }
        this.objective = objective;
    }

    @Override
    public String executeMission() {

        return (this.pilotName
                + ": "
                + this.aircraft.getClass().getSimpleName()
                + " taking pictures of "
                + this.objective
                + " with: "
                + ((IVerificationAircraft) this.aircraft).camera());
    }

}
