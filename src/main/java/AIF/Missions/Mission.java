package AIF.Missions;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class Mission {
    protected String pilotName;
    protected AerialVehicle aircraft;
    protected Coordinates destination;

    public Mission(String pilotName, AerialVehicle aircraft, Coordinates destination) {
        this.pilotName = pilotName;
        this.aircraft = aircraft;
        this.destination = destination;
    }

    public void begin() {
        System.out.println("Beginning mission!");
        this.aircraft.flyTo(destination);

    }

    public void cancel() {
        System.out.println("Abort mission!");
        this.aircraft.land(aircraft.base());
    }

    public void finish() {
        System.out.println(executeMission());
        this.aircraft.land(aircraft.base());
        System.out.println("Finish mission!");
    }

    abstract String executeMission();


}
